import React, { Component } from 'react';

const Pagination = (props) => {

	return(

		<nav className="mt-4 mb-3">
		    <ul className="pagination justify-content-center mb-0">
		        <li className="page-item ">
		            <a className="page-link first" href="#">
		                <i className="simple-icon-control-start"></i>
		            </a>
		        </li>
		        <li className="page-item ">
		            <a className="page-link prev" href="#">
		                <i className="simple-icon-arrow-left"></i>
		            </a>
		        </li>
		        <li className="page-item active">
		            <a className="page-link" href="#">1</a>
		        </li>
		        <li className="page-item ">
		            <a className="page-link" href="#">2</a>
		        </li>
		        <li className="page-item">
		            <a className="page-link" href="#">3</a>
		        </li>
		        <li className="page-item ">
		            <a className="page-link next" href="#" aria-label="Next">
		                <i className="simple-icon-arrow-right"></i>
		            </a>
		        </li>
		        <li className="page-item ">
		            <a className="page-link last" href="#">
		                <i className="simple-icon-control-end"></i>
		            </a>
		        </li>
		    </ul>
		</nav>

	)

} 

export default Pagination;


