import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import FileSaver from 'file-saver';

import PageHeader from '../../components/PageHeader';
import NoResult from '../../components/NoResult';

import { getFiles, deleteFile } from '../../actions/files';
import notify from '../../utils/notify';

import UploadFileModal from './New.File';
import Moment from 'react-moment';
import { API_URL } from '../../config';
import axios from 'axios';

import EditFileModal from './Edit.File';

const FileCard = (props) => {
	let { name, createdAt, _id, type } = props.file;

	return(
		<div className="card d-flex flex-row mb-3">
            <div className="d-flex flex-grow-1 min-width-zero">
                <div className="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                    <a className="list-item-heading mb-1 truncate w-40 w-xs-100" href="#">
                        <i className="simple-icon-folder"></i> { name }
                    </a>
                    
                    <p className="mb-0 text-muted text-small w-15 w-xs-100 text-right">
                    	<Moment format="YYYY/MM/DD hh:mm">{ createdAt }</Moment>
                    </p>
                    <p className="mb-0 text-muted text-small w-30 w-xs-100 text-right">
                    	<button type="button" className="btn btn-primary btn-xs file-action download" onClick={ () => { props.downloadFile(_id, name, type) } }>Download</button>
                    	<button type="button" className="btn btn-danger btn-xs file-action delete" onClick={ () => { props.onDeleteFile(_id) } }>DELETE</button>
	            		<button type="button" className="btn btn-dark btn-xs file-action rename" onClick={ () => { props.renameFile( props.index ) } }>RENAME</button>
                    </p>
                </div>
            </div>
        </div>
	)

}


class Files extends Component {


	constructor(props){
		super(props);
		this.state = {
			selectedFile: null
		}

	}

	uploadFile  = () => {

		if( window.mydropzone )
			window.mydropzone.removeAllFiles();

		window.$("#newFileModal").modal('show');
	}

	componentWillMount(){

		this.props.getFiles( this.props.match.params.folder );

	}

	onUploadSuccess = ( res ) => {

		if( res.status ){
			notify.success( "Success", res.message );
			
			this.props.getFiles( this.props.match.params.folder );

			setTimeout( () => {
				window.$("#newFileModal").modal('hide');
			}, 500 )

		}
		else
		{
			notify.error( "Error", res.message );
		}

	}

	componentWillReceiveProps(props){

		if( props.error && props.error !== null){
			notify.error("Error", this.props.error)
		}

		//check for response in store
		if( props.response !== null  ){
			let { status, message } = props.response;

			if( !status ){
				notify.error( "Error", message );
			}

			if( status ){
				notify.success( "Success", message );
				//this.hideAnyModal(); //for new and edit modal
				this.props.getFiles( this.props.match.params.folder );
			}
		}
	}	

	deleteFile = (id) => {

		let _confirm = window.confirm("Are you sure?");
		if( _confirm ){

			this.props.deleteFile(id);
		}
	}

	renameFile = (index) => {

		this.setState( { selectedFile: this.props.records.files[index] } )

		window.$("#editFileModal").modal('show');
	}

	//return a promise that resolves with a File instance
	// urltoFile = (url, filename, mimeType) => {
	//     return (fetch(url)
	//         .then(function(res){return res.arrayBuffer();})
	//         .then(function(buf){return new File([buf], filename, {type:mimeType});})
	//     );
	// }

	downloadFile = (id, name, type) => {

		axios.get( API_URL + '/files/'+ id +'/download', { headers: { 'Authorization': 'Bearer ' + window.localStorage.getItem('jwt') } } )
			.then( (res) => {

				var element = document.createElement('a');
				element.setAttribute('href', 'data:' + type + ';base64,' + res.data);
				element.setAttribute('download', name);

				element.style.display = 'none';
				document.body.appendChild(element);

				element.click();

				document.body.removeChild(element);

				// this.urltoFile('data:' + type + ';base64,'+res.data, name, res.headers['content-type'])
				// .then(function(file){
				//     console.log(file);
				// })

				// var blob = new Blob([res.data], {type: res.headers['content-type']});
				// FileSaver.saveAs(blob, name);

	        } )
	}

	render() {

		let bulkActions = [
	  		//{ title: 'Delete', callback: this.bulkDelete }
	  	]

	  	let actions = [
	  		{ title: 'Upload File', callback: this.uploadFile }
	  	]

	  	let sortActions = [
	  		//{ title: 'Name', callback: this.sortBy }
	  	]

	  	let { records } = this.props

		return (

			<div>
				{ records && records.folder && <PageHeader 
		        	title={ records.folder.name }
		        	actions={actions} 
		        	bulkActions={bulkActions} 
		        	sortActions={sortActions}
		        	onChangePerPage={ this.changePerPage }
		        	pagination={ false } /> }

		        <div className="col--12 list" data-check-all="checkAll">

	        		{ records && records.files && records.files.length == 0 && 
		        			<NoResult />
	        		}

	            	{ records && records.files && records.files.length > 0 && 

	            		_.map( records.files, (file , k) => {
	            			return <FileCard index={k} file={file} key={k} onDeleteFile={ this.deleteFile } renameFile={ this.renameFile } downloadFile={ this.downloadFile } />
	            		})
	            	}

	            </div>

	            { records && records.folder && <UploadFileModal onFIleUpload={ this.onUploadSuccess } folder={ records.folder } /> }

	            <EditFileModal file={ this.state.selectedFile } />

	        </div>
    	)
	}

}

/* Redux */
const mapStateToProps = (store) => {
    return {
    	records: store.files.records,
        response: store.files.response,
        error: store.files.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getFiles: ( slug ) => dispatch( getFiles( slug ) ),
        deleteFile: (id) => dispatch( deleteFile(id) )
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Files)