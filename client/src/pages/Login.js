import React, { Component } from 'react';

import { googleLogin } from '../actions/auth';
import { connect } from 'react-redux';

import { API_URL } from '../config';
import axios from 'axios';

import { startLoader } from '../utils/loader';


class Login extends Component {

    componentWillReceiveProps(){

        setTimeout( () => {
            //window.location.href = this.props.auth.auth_url;
            let data = this.props.auth.response;

            if( data && data.status ){

                //this.setState( {is_loading: false} )

                window.localStorage.setItem('jwt', data.token );
                window.location.href = '/';

            }    

        } )

    }

    componentWillMount(){
        

    }

    componentDidMount(){
        setTimeout( () => {

            var url = new URL(window.location.href);
            var code = url.searchParams.get('code');

            if( code && code != undefined ){
                startLoader();
                this.props.googleLogin(code);
            }

        } )
    }

    getGoogleLoginUrl = () => {
            
            axios.get( API_URL + '/google/login' ).then( (res) => {
                
                if( res.data.status )
                    window.location.href = res.data.url;

            } )

    }

    render() {

        return (
            <div className="login-main">
                {/*<div className="fixed-background"></div>*/}
              	<div className="container">

                    <div className='card auth-card mx-auto' >
                       
                        <div className="form-side" style={ { width: '100%'} }>
                            <a href="Dashboard.Default.html">
                                <span className="logo-single"></span>
                            </a>
                            <h6 className="mb-4">Login</h6>
                            <form>
                                <label className="form-group has-float-label mb-4">
                                    <input className="form-control" />
                                    <span>E-mail</span>
                                </label>

                                <label className="form-group has-float-label mb-4">
                                    <input className="form-control" type="password" placeholder="" />
                                    <span>Password</span>
                                </label>
                                <div className="d-flex justify-content-between align-items-center">
                                    <a href="#">Forget password?</a>
                                    <button className="btn btn-primary btn-lg btn-shadow btn-login" type="submit">LOGIN</button>
                                </div>
                            </form>
                            <button className="btn-google btn-block btn mt-4 btn-shadow" onClick={ this.getGoogleLoginUrl }>
                                <i className="simple-icon-social-google"></i> Login with Google
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


/* Redux */
const mapStateToProps = (store) => {
    return {
        auth: store.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        googleLogin: (code) => dispatch( googleLogin(code) )
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)