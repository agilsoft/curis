const auth = require('../google-auth');

/*** Database */
const db = require('../db');
const User = db.model('User');
var jwt = require('jsonwebtoken');

const SECRET = require('../constants').SECRET;

module.exports = function(app){
	/**
	* TO get a URL to perform google auth
	*/
	app.get('/google/login', (req, res) => {

		let url = auth.urlGoogle();
		res.send( { status: true, url: url } );
	})

	/**
	* Google Callback on successfull login
	**/

	app.get('/google/auth', async(req, res) => {
		try{

			//get user profile from google
			const profile = await auth.getGoogleAccountFromCode(req.query.code);

			//get user from db using email
			let user = await User.findOne( {email: profile.email} );

			//create new use if not exists
			if( user === null ){ //user does not exists create a new record
				user = new User();
				user.google_id = profile.id;
				user.email = profile.email;
				user.is_google = true;
			}

			//update tokens
			// user.access_token = profile.tokens.access_token;
			// user.refresh_token = profile.tokens.refresh_token;
			await user.save();

			let _payload = { 
				user_id: user.id
			}

			let token = jwt.sign( _payload , SECRET); 
			res.send( {status: true, message: "Login Successfull", data: user, token: token} )

		}
		catch(e){
			res.send( { status: false, message: e.message} );
		}

	});
}