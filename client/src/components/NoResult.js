import React  from 'react';

const NoResult = () => {
	return(
		<div className="no-result">
			<span>No result found.</span>
		</div>
	)
}


export default NoResult;