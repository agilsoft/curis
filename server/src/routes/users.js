var mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const db = require('../db');
const User = db.model('User');
const saltRounds = 10;

module.exports = function(app){

	/* Get all registered users */
	app.get('/users', async(req, res) => {

		try{

			if( ! req.current_user.admin ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			users = await User.find();
			res.send( { status: true, data: users } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	})

	/* View User **/

	app.get('/users/profile', async(req, res) => {

		try{
			res.send( { status: true, data: req.current_user } ); 
		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	})


	/***
	* Update my Profile
	*/
	app.put('/users/profile', async(req, res) => {

		try{
			
			let user = await User.findOne( {_id: req.current_user._id } );

			//check if user exists in db
			if( user === null ){
				return res.send( { status: false, message: "User not Found" } );
			}

			//update user data
			user.firstname = req.body.firstname;
			user.lastname = req.body.lastname;
			user.phone = req.body.phone;
			user.address = req.body.address;
			user.address2 = req.body.address2;
			user.city = req.body.city;
			user.state = req.body.state;
			user.zip = req.body.zip;

			await user.save();

			res.send( { status: true, message: "Proile updated successfully" } ); 

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	})

	/*** 
	* Update user profile
	* params: firstname, lastname, phone
	*/
	app.put('/users/profile/:id', async(req, res) => {

		try{

			if( req.current_user._id.toString() !== req.params.id.toString() && !req.current_user.admin ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			//check if valid mongo id
			if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
				return res.send( { status: false, message: "invalid Id" } );
			}

			
			let user = await User.findOne( {_id: req.params.id } );

			//check if user exists in db
			if( user === null ){
				return res.send( { status: false, message: "User not Found" } );
			}

			//update user data
			user.firstname = req.body.firstname;
			user.lastname = req.body.lastname;
			user.phone = req.body.phone;
			user.address = req.body.address1;
			user.address1 = req.body.address1;
			user.city = req.body.city;
			user.state = req.body.state;
			user.zip = req.body.zip;

			await user.save();

			res.send( { status: true, message: "Proile updated successfully", data: user } ); 

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

	app.put('/users/password', async(req, res) => {

		try{

			let user = await User.findOne( {_id: req.current_user._id } );

			//check old password for users not registered through google
			if( !req.current_user.is_google ){

				let valid = await bcrypt.compare( req.body.current, user.password );

				if( !valid ){
					return res.send( { status: false, message: "Current password is invalid." } );
				}
			}

			//compare new password and confirm password
			if( req.body.new !== req.body.confirm ){
				return res.send( { status: false, message: "Confirm password should match new password." } );
			}



			let hash = await bcrypt.hash(req.body.new, saltRounds);

			user.password = hash;
			user.is_google = false; //mark is_google as false if password generated

			user.save();
			return res.send( { status: true, message: "Password successfully updated." } );

		}
		catch(e){
			res.send( { status: false, message: e.message } )
		}

	});

}