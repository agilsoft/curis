import { API_URL } from '../config';
import axios from 'axios';

import { FILES as TYPE } from '../constants';

/*** Authorization Header */
let axiosConfig = {
  	headers: {
      'Authorization': 'Bearer ' + window.localStorage.getItem('jwt')
    }
};


export function getFiles(folder){
	return {
		type: `${TYPE}`,
		payload: axios.get( API_URL + '/folders/'+ folder +'/files', axiosConfig )
	}
}


export function renameFile(id, name){
	return {
		type: `${TYPE}_EDIT`,
		payload: axios.put( API_URL + '/files/' + id, { name: name } , axiosConfig )
	}
}


export function deleteFile(id){
	return {
		type: `${TYPE}_DELETE`,
		payload: axios.delete( API_URL + '/files/' + id , axiosConfig )
	}
}
