const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const ObjectId = Schema.ObjectId;
 
let schema = new Schema({
	firstname: String,
	lastname: String,
	email: String,
	phone: String,
	address: String,
	address2: String,
	city: String,
	state: String,
	zip: String,
	is_google: Boolean,
	password: String
	// access_token: String,
	// refresh_token: String
},
{
  	timestamps: true
});


schema.pre('save', function(next) {
  this.increment();
  return next();
});


module.exports = schema;