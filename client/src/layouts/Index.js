import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import Login from '../pages/Login';

class Index extends Component {
	render() {
	    return (
	    	<Router>
			    <Route exact path="/login" component={ Login } />
	    	</Router>
	    )
	}
}

export default Index;