import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    FormBuilder,
    FieldGroup,
    FieldControl,
    Validators,
 } from "react-reactive-form";

import { TextInputPlain as TextInput, SelectInput } from '../../utils/inputs';

import { updateProfile, getMyProfile } from '../../actions/auth';
import notify from '../../utils/notify';

class Account extends Component {

	constructor(props){
		super(props);

		this.userForm = FormBuilder.group({
	        firstname: ["", Validators.required],
	        lastname: ["", Validators.required],
	        email: ["", Validators.required],
	        phone: ["", Validators.required],
	        address: ["", Validators.required],
	        address2: ["", Validators.required],
	        city: ["", Validators.required],
	        state: ["", Validators.required],
	        zip: ["", Validators.required]
	    });
	    //this.initForm(props.user)

	}

	componentWillReceiveProps(props){
	
		if( props.error && props.error !== null){
			notify.error("Error", this.props.error)
		}

		//check for response in store
		if( props.response !== null  ){
			let { status, message } = props.response;

			if( !status ){
				notify.error( "Error", message );
			}

			if( status ){
				notify.success( "Success", message );
				props.getMyProfile();
			}
		}

	}

	componentWillMount(){
		this.initForm( this.props.user )
	}

	initForm(user){
		if( user !== null ){
			this.userForm.setValue( {
				firstname: user.firstname ? user.firstname : "",
				lastname: user.lastname ? user.lastname : "",
				email: user.email ? user.email : "",
				phone: user.phone ? user.phone : "",
				address : user.address ? user.address : "",
				address2: user.address2 ? user.address2 : "",
				city: user.city ? user.city : "",
				state: user.state ? user.state : "",
				zip: user.zip ? user.zip : ""
			} )
		}
		
	}

	handleSubmit = (e) => {

		e.preventDefault();

		
		if(this.userForm.status == "VALID"){
			this.props.updateProfile( this.userForm.value );
		}

		

	}

	render() {

	    return (

	    	<div>
	    		<h1>Profile</h1>
	            <div className="separator mb-5"></div>
	      		<div className="card mb-4">
		            <div className="card-body">
		            	<FieldGroup
			                control={this.userForm}
			                render={({ get, invalid }) => (
			                  	<form id="editAccount" onSubmit={this.handleSubmit}>
				                  	<div className="form-row">
				                  		<div className="form-group col-md-6">
			                     			<FieldControl
						                      name="firstname"
						                      render={ TextInput }
						                      meta={{ label: "First Name", id: "firstname" }}
						                    />
					                    </div>

					                    <div className="form-group col-md-6">
			                     			<FieldControl
						                      name="lastname"
						                      render={ TextInput }
						                      meta={{ label: "Last Name", id: "lastname" }}
						                    />
					                    </div>
					                </div>
					                <div className="form-row">
				                  		<div className="form-group col-md-6">
			                     			<FieldControl
						                      name="email"
						                      render={ TextInput }
						                      disabled={true}
						                      meta={{ label: "Email Address", id: "email"}}
						                    />
					                    </div>

					                    <div className="form-group col-md-6">
			                     			<FieldControl
						                      name="phone"
						                      render={ TextInput }
						                      meta={{ label: "Phone", id: "phone" }}
						                    />
					                    </div>
					                </div>
					                <div className="form-row">
				                  		<div className="form-group col-md-6">
			                     			<FieldControl
						                      name="address"
						                      render={ TextInput }
						                      meta={{ label: "Address", id: "address" }}
						                    />
					                    </div>
					                    <div className="form-group col-md-6">
			                     			<FieldControl
						                      name="address2"
						                      render={ TextInput }
						                      meta={{ label: "Address 2", id: "address2" }}
						                    />
					                    </div>
					                </div>
					                <div className="form-row">
					                	<div className="form-group col-md-4">
			                     			<FieldControl
						                      name="city"
						                      render={ TextInput }
						                      meta={{ label: "City", id: "city" }}
						                    />
					                    </div>
					                    <div className="form-group col-md-4">
			                     			<FieldControl
						                      name="state"
						                      render={ TextInput }
						                      meta={{ label: "State", id: "state" }}
						                    />
					                    </div>
					                    <div className="form-group col-md-4">
			                     			<FieldControl
						                      name="zip"
						                      render={ TextInput }
						                      meta={{ label: "Zip", id: "zip" }}
						                    />
					                    </div>
					                </div>

					                <button disabled={invalid} type="submit" className="save btn btn-primary d-block mt-3">Save</button>
	                        	</form>
                        	) } />
		            </div>
		        </div>
		    </div>
	    );
	}
}

const mapStateToProps = (store) => {
    return {
        user: store.auth.data,
        error: store.auth.error,
        response: store.auth.response
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProfile: (profile) => dispatch( updateProfile(profile) ),
        getMyProfile: () => dispatch( getMyProfile() )
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Account);