import {combineReducers} from "redux"


import auth from './auth';
import folders from './folders';
import files from './files';

export default combineReducers({
	auth,
	folders,
	files
})