import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Link } from "react-router-dom";

class Nav extends Component {
  render() {
    return (
      	<nav className="navbar fixed-top">
	        <a href="#" className="menu-button d-none d-md-block">
	            {/*<svg className="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
	                <rect x="0.48" y="0.5" width="7" height="1" />
	                <rect x="0.48" y="7.5" width="7" height="1" />
	                <rect x="0.48" y="15.5" width="7" height="1" />
	            </svg>
	            <svg className="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
	                <rect x="1.56" y="0.5" width="16" height="1" />
	                <rect x="1.56" y="7.5" width="16" height="1" />
	                <rect x="1.56" y="15.5" width="16" height="1" />
	            </svg>*/}
	            <img src="/hamburgerCuris.png" />
	        </a>

	        <a href="#" className="menu-button-mobile d-xs-block d-sm-block d-md-none">
	            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
	                <rect x="0.5" y="0.5" width="25" height="1" />
	                <rect x="0.5" y="7.5" width="25" height="1" />
	                <rect x="0.5" y="15.5" width="25" height="1" />
	            </svg>
	        </a>

	        <div className="search" data-search-path="Layouts.Search.html?q=">
	            <input placeholder="Search..." />
	            <span className="search-icon">
	                <i className="simple-icon-magnifier"></i>
	            </span>
	        </div>

	        <Link to="/" className="navbar-logo">
	        	<span className="logo d-none d-xs-block"></span>
	            <span className="logo-mobile d-block d-xs-none"></span>
	        </Link>

	        <div className="ml-auto">
	            <div className="header-icons d-inline-block align-middle">
	                

	            </div>

	            <div className="user d-inline-block">
	                <button className="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true"
	                    aria-expanded="false">
	                    <span className="name">{ this.props.user && this.props.user.email }</span>
	                </button>
	                
	            </div>
	        </div>
	    </nav>
    );
  }
}

/* Redux */
const mapStateToProps = (store) => {
    return {
        user: store.auth.data
    }
}

export default connect(
  mapStateToProps,
  null
)(Nav)