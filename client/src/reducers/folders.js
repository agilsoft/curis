import { FOLDERS as TYPE } from '../constants';


export default function reducer( state  = {
	error: null,
	records: null,
	response: null
}, action){

	switch(action.type){

		case `${TYPE}_PENDING`:
			return {...state, error: null}
		case `${TYPE}_FULFILLED`:
			return {...state, records: action.payload.data.data, error: null, response:null }
		case `${TYPE}_REJECTED`:
			return {...state, error: action.payload }

		case `${TYPE}_CREATE_PENDING`:
			return {...state, error: null}
		case `${TYPE}_CREATE_FULFILLED`:
			return {...state, response: action.payload.data, error: null }
		case `${TYPE}_CREATE_REJECTED`:
			return {...state, error: action.payload }

		case `${TYPE}_DELETE_PENDING`:
			return {...state, error: null}
		case `${TYPE}_DELETE_FULFILLED`:
			return {...state, response: action.payload.data, error: null }
		case `${TYPE}_DELETE_REJECTED`:
			return {...state, error: action.payload }

		case `${TYPE}_EDIT_PENDING`:
			return {...state, error: null}
		case `${TYPE}_EDIT_FULFILLED`:
			return {...state, response: action.payload.data, error: null }
		case `${TYPE}_EDIT_REJECTED`:
			return {...state, error: action.payload }


	}

	return state;

}