const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/file_manager');

//Defining Models
mongoose.model('User', require('./models/user'))
mongoose.model('Folder', require('./models/folder'))
mongoose.model('File', require('./models/file'))

module.exports = mongoose;
