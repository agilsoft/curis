import React, { Component } from 'react';

import { connect } from 'react-redux';

import {
    FormBuilder,
    FieldGroup,
    FieldControl,
    Validators,
 } from "react-reactive-form";

 import { TextInput } from '../../utils/inputs';

 import { renameFile } from '../../actions/files';

class Edit extends Component {

	constructor(props){
		super(props);
		this.fileForm = FormBuilder.group({
	        name: ["", Validators.required]
	    });
	}

	handleSubmit = (e) => {
		e.preventDefault();

		if(this.fileForm.status == "VALID"){

			let { name } = this.fileForm.value;
			this.props.renameFile( this.props.file._id, name );
		}

	}

	componentWillReceiveProps(props){
		
		if( props.file !== null ){
			this.fileForm.setValue( { name: props.file.name } )
		}
		
	}

	render(){
		return(
			<div className="modal fade" id="editFileModal" tabIndex="-1" role="dialog" aria-hidden="true">
	            <div className="modal-dialog" role="document">
	                <div className="modal-content">
	                    <div className="modal-header">
	                        <h5 className="modal-title" id="exampleModalLabel">Rename File</h5>
	                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div className="modal-body">

	                    	<FieldGroup
			                control={this.fileForm}
			                render={({ get, invalid }) => (
			                  	<form id="renameFile" onSubmit={this.handleSubmit}>

	                     			<FieldControl
				                      name="name"
				                      render={ TextInput }
				                      meta={{ label: "File Name" }}
				                    />
	                        	</form>
                        	) } />

	                    </div>
	                    <div className="modal-footer">
	                        <button type="submit" form="renameFile" className="btn btn-primary">Rename</button>
	                    </div>
	                </div>
	            </div>
	        </div>
		)
	}


}


const mapDispatchToProps = (dispatch) => {
    return {
        renameFile: (id, name) => dispatch( renameFile(id, name) )
    }
}

export default connect(
  null,
  mapDispatchToProps
)(Edit);