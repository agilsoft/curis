import { API_URL } from '../config';
import axios from 'axios';

import { FOLDERS as TYPE } from '../constants';

/*** Authorization Header */
let axiosConfig = {
  	headers: {
      'Authorization': 'Bearer ' + window.localStorage.getItem('jwt')
    }
};

export function getMyFolders(code){
	return {
		type: `${TYPE}`,
		payload: axios.get( API_URL + '/folders', axiosConfig )
	}
}


export function createNewFolder(name){
	return {
		type: `${TYPE}_CREATE`,
		payload: axios.post( API_URL + '/folders', {name: name}, axiosConfig )
	}
}


export function deleteFolder(id){
	return {
		type: `${TYPE}_DELETE`,
		payload: axios.delete( API_URL + '/folders/' + id , axiosConfig )
	}
}

export function renameFolder(id, name){
	return {
		type: `${TYPE}_EDIT`,
		payload: axios.put( API_URL + '/folders/' + id, { name: name } , axiosConfig )
	}
}
