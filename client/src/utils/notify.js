
export default {
	info: (title, message) => {

		window.$.notify( {
			title: title,
			message: message
		},{
			type: 'info',
			'z_index': 1060,
			'placement': {
				'from': 'bottom'
			}
		} )

	},
	error: (title, message) => {

		window.$.notify( {
			title: title,
			message: message
		},{
			type: 'danger',
			'z_index': 1060,
			'placement': {
				'from': 'bottom'
			}
		} )

	},
	success: (title, message) => {

		window.$.notify( {
			title: title,
			message: message
		},{
			type: 'success',
			'z_index': 1060,
			'placement': {
				'from': 'bottom'
			}
		} )

	},
	warn: (title, message) => {

		window.$.notify( {
			title: title,
			message: message
		},{
			type: 'warning',
			'z_index': 1060,
			'placement': {
				'from': 'bottom'
			}
		} )

	}
}
