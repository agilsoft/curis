import React, { Component } from 'react';
import './App.css';

import Dashboard from './layouts/Dashboard';
import Index from './layouts/Index';

import { Provider } from "react-redux";
import store from './store';

import emitter from './utils/emitter';

const Layout = function(){

  let isLogedIn = false;
  let jwt = window.localStorage.getItem('jwt');

  if( jwt && jwt !== null ){
    isLogedIn = true;
  }

  return isLogedIn ? <Dashboard /> : <Index />
}

const Loader = function(){
  return(

    <div className="site-loader">
      <div className="loading"></div>
    </div>

  )
}

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      is_loading: false
    }
  }

  componentWillMount(){
      
    emitter.addListener('loading:start', (...args) => {
        this.setState( { is_loading: true } )
    });

    emitter.addListener('loading:stop', (...args) => {
        this.setState( { is_loading: false } )
    });
  }

  render() {
    return (
    	 <Provider store={store}>
    		  <div>
            <Layout />
            { this.state.is_loading && <Loader /> }
          </div>
      	</Provider>
    );
  }
}

export default App;
