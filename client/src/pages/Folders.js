import React, { Component } from 'react';
import _ from 'lodash';

import PageHeader from '../components/PageHeader';
//import Pagination from '../components/Pagination';
import NoResult from '../components/NoResult';
import { connect } from 'react-redux';

import { getMyFolders, deleteFolder } from '../actions/folders';
import notify from '../utils/notify';

import Moment from 'react-moment';
import NewFolderModal from './folders/New.Folder';
import EditFolderModal from './folders/Edit.Folder';

import { Link } from "react-router-dom";

const FolderCard = (props) => {
	let { name, createdAt, _id, slug } = props.folder

	return (
		<div className="col-xxl-4 col-xl-4 col-12">
			<div className="card d-flex flex-row mb-4 media-thumb-container">
	            <Link to={ `/files/${slug}` } className="d-flex align-self-center media-thumbnail-icon">
	                <i className="iconsmind-Folder-Open"></i>
	            </Link>
	            <div className="d-flex flex-grow-1 min-width-zero">
	                <div className="card-body align-self-center d-flex flex-column justify-content-between min-width-zero align-items-lg-center">
	                    <Link to={ `/files/${slug}` } className="w-100">
	                        <p className="list-item-heading mb-1 truncate">{ name }</p>
	                    </Link>
	                    <p className="mb-1 text-muted text-small w-100 truncate">
	                    	<Moment format="YYYY/MM/DD hh:mm">{ createdAt }</Moment>
	                    </p>
	                </div>

	            </div>
	            <div className="folder-actions">
	            	<button type="button" className="btn btn-danger btn-xs folder-action" onClick={ () => { props.onDeleteFolder(_id) } }>DELETE</button>
	            	<button type="button" className="btn btn-dark btn-xs folder-action" onClick={ () => { props.renameFolder( props.index ) } }>RENAME</button>
	            </div>
	        </div>
	    </div>
    );
}

class Folders extends Component {

	constructor(props){
		super(props);
		this.state = {
			selectedFolder: null
		}
	}

	addNewFolder = () => {
		window.$("#newFolderModal").modal('show');
	}

	sortBy = () => {}
	changePerPage = () => {}
	bulkDelete = () => {}

	hideAnyModal = () => {
		window.$(".modal").modal('hide');
	}

	deleteFolder = (id) => {
		let _confirm = window.confirm("Are you sure?");
		if( _confirm ){
			this.props.deleteFolder(id);
		}
	}

	renameFolder = (index) => {

		this.setState( { selectedFolder: this.props.records[index] } )

		window.$("#editFolderModal").modal('show');
	}

	componentWillMount(){
		this.props.getMyFolders();
	}

	componentWillReceiveProps(props){

		if( props.error && props.error !== null){
			notify.error("Error", this.props.error)
		}

		//check for response in store
		if( props.response !== null  ){
			let { status, message } = props.response;

			if( !status ){
				notify.error( "Error", message );
			}

			if( status ){
				notify.success( "Success", message );
				this.hideAnyModal(); //for new and edit modal
				props.getMyFolders();
			}
		}
	}	
  
	render() {

		let records = this.props.records;

	  	let bulkActions = [
	  		//{ title: 'Delete', callback: this.bulkDelete }
	  	]

	  	let actions = [
	  		{ title: 'New Folder', callback: this.addNewFolder }
	  	]

	  	let sortActions = [
	  		//{ title: 'Name', callback: this.sortBy }
	  	]

	    return (
	    	<div className="library-app">
		        
		        <PageHeader 
		        	title="Folders"
		        	actions={actions} 
		        	bulkActions={bulkActions} 
		        	sortActions={sortActions}
		        	onChangePerPage={ this.changePerPage }
		        	pagination={ false } />

		        <div className="row" data-check-all="checkAll">

		        		{ records && records.length == 0 && 
		        			<NoResult />
		        		}

		            	{ records && records.length > 0 && 

		            		_.map( records, (folder , k) => {
		            			return <FolderCard index={k} folder={folder} key={k} onDeleteFolder={ this.deleteFolder } renameFolder={ this.renameFolder } />
		            		})
		            	}

		            	{ /* <Pagination /> */ }

		        </div>

		        <NewFolderModal />

		        <EditFolderModal folder={ this.state.selectedFolder } />

	        </div>
	    );
	}
}

/* Redux */
const mapStateToProps = (store) => {
    return {
    	records: store.folders.records,
        response: store.folders.response,
        error: store.folders.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMyFolders: () => dispatch( getMyFolders() ),
        deleteFolder: (id) => dispatch( deleteFolder(id) )
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Folders)
