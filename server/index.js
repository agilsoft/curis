const express = require('express')
const app = express();
const bodyParser = require('body-parser');

var jwt = require('jsonwebtoken');
var jwt_auth = require('express-jwt');
const SECRET = require('./src/constants').SECRET;

const db = require('./src/db');
const User = db.model('User');

var cors = require('cors');


//allow cors
app.use(cors())

//apply authentication
app.use( jwt_auth({ secret: SECRET})
	.unless({ path: [
		'/google/login',
		'/google/auth'
	]})
);

//check authentication
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send({status: false, message: 'You are not authorized to access this resource'});
  }
});

//current user
app.use( async function(req, res, next) {

  if( req.headers.authorization ){
  	
  	let _arr = req.headers.authorization.split(" ");
  	let token = _arr[1];

  	let _user = jwt.verify(token, SECRET);

  	let user = await User.findOne({ _id: _user.user_id });

  	if( user == null ){
  		return res.send({status: false, message: 'Invalid user'});
  	}

  	//set current user in req
  	req.current_user = user;
  }
  next();
})


// support parsing of application/json type post data
app.use(bodyParser.json());
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

const port = 3000;

app.get('/', (req, res) => res.send('App is working'))

/**- inlcude routes **/
require('./src/routes/google')(app);
require('./src/routes/users')(app);
require('./src/routes/folders')(app);
require('./src/routes/files')(app);

app.listen(port, () => console.log(`App listening on ${port}!`))