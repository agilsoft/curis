const google = require('googleapis').google;

/**
 * This scope tells google what information we want to request.
 */
const defaultScope = [
  'https://www.googleapis.com/auth/plus.me',
  'https://www.googleapis.com/auth/userinfo.email',
];

const googleConfig = {
  clientId: '1095980451989-j881jpgooi9ie47r9htf65nv7o3qoums.apps.googleusercontent.com', // e.g. asdfghjkljhgfdsghjk.apps.googleusercontent.com
  clientSecret: 'G2Zo6hU8fjErfl1kjOdm_NQQ', // e.g. _ASDFA%DFASDFASDFASD#FAD-
  redirect: 'http://192.168.1.84:3001/login' // this must match your google api settings
};

/**
 * Create the google auth object which gives us access to talk to google's apis.
 */
createConnection = () => {
  return new google.auth.OAuth2(
    googleConfig.clientId,
    googleConfig.clientSecret,
    googleConfig.redirect
  );
}

/**
 * Get a url which will open the google sign-in page and request access to the scope provided (such as calendar events).
 */
getConnectionUrl = (auth) => {
  return auth.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent', // access type and approval prompt will force a new refresh token to be made each time signs in
    scope: defaultScope
  });
}

/**
 * Helper function to get the library with access to the google plus api.
 */
getGooglePlusApi = (auth) => {
  return google.plus({ version: 'v1', auth });
}

module.exports = {

	/**
	 * Create the google url to be sent to the client.
	 */
	urlGoogle: () => {
	  const auth = createConnection(); // this is from previous step
	  const url = getConnectionUrl(auth);
	  return url;
	},

	/**
	 * Extract the email and id of the google account from the "code" parameter.
	 */
	getGoogleAccountFromCode: async(code) => {
	  
	  const auth = createConnection();

	  // get the auth "tokens" from the request
	  const data = await auth.getToken(code);
	  const tokens = data.tokens;
	  
	  // add the tokens to the google api so we have access to the account
	  auth.setCredentials(tokens);
	  
	  // connect to google plus - need this to get the user's email
	  const plus = getGooglePlusApi(auth);
	  const me = await plus.people.get({ userId: 'me' });
	  
	  // get the google id and email
	  const userGoogleId = me.data.id;
	  const userGoogleEmail = me.data.emails && me.data.emails.length && me.data.emails[0].value;

	  // return so we can login or sign up the user
	  return {
	    id: userGoogleId,
	    email: userGoogleEmail,
	    tokens: tokens, // you can save these to the user if you ever want to get their details without making them log in again
	  };
	}

}