var mongoose = require('mongoose');
var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })

const db = require('../db');
const Folder = db.model('Folder');
const File = db.model('File');

module.exports = function(app){

	/****** 
	** List all folders 
	*****/
	app.get('/folders', async(req, res) => {

		try{	

			let folders = await Folder.find({user: req.current_user._id}).sort({createdAt: 'desc'});
			res.send( { status: true, data: folders } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

	/*******
	** View Folder
	******/
	app.get('/folders/:id', async(req, res) => {

		try{	

			//check if valid mongo id
			if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
				return res.send( { status: false, message: "invalid Id" } );
			}

			let folder = await Folder.findOne( {_id: req.params.id } );

			//check if folder exists in db
			if( folder === null ){
				return res.send( { status: false, message: "Folder not Found" } );
			}

			//check if owner
			if( req.current_user._id.toString() !== folder.user.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			res.send( { status: true, data: folder } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});


	/***** 
	/* Create new folder 
	/* params:  name
	******/
	app.post('/folders', async(req, res) => {

		try{	

			//check for duplicate name
			let _folder = await Folder.findOne( {name: req.body.name, user: req.current_user._id } )

			if( _folder !== null ){
				return res.send( { status: false, message: "Folder already exists." } );
			}

			let folder = new Folder();
			folder.name = req.body.name;
			folder.user = req.current_user._id;
			await folder.save();

			res.send( { status: true, message: "Folder successfully created", data: folder } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	})

	/**** 
	/* Update folder
	/* params:  name
	***********/
	app.put('/folders/:id', async(req, res) => {

		try{	

			//check if valid mongo id
			if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
				return res.send( { status: false, message: "invalid Id" } );
			}

			let folder = await Folder.findOne( {_id: req.params.id } );

			//check if folder exists in db
			if( folder === null ){
				return res.send( { status: false, message: "Folder not Found" } );
			}

			let _folder = await Folder.findOne( {name: req.body.name, user: req.current_user._id } )
			if( _folder !== null && _folder._id.toString() !== folder._id.toString() ){
				return res.send( { status: false, message: "Folder already exists." } );
			}

			//check if owner
			if( req.current_user._id.toString() !== folder.user.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			folder.name = req.body.name;
			await folder.save();

			res.send( { status: true, message: "Folder successfully updated", data: folder } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	})

	/**** 
	/* Delete folder
	***********/
	app.delete('/folders/:id', async(req, res) => {

		try{

			//check if valid mongo id
			if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
				return res.send( { status: false, message: "invalid Id" } );
			}

			let folder = await Folder.findOne( {_id: req.params.id } );

			//check if folder exists in db
			if( folder === null ){
				return res.send( { status: false, message: "Folder not Found" } );
			}

			//check if owner
			if( req.current_user._id.toString() !== folder.user.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			await folder.remove();

			res.send( { status: true, message: "Folder successfully deleted" } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

	/******
	* Upload file to folder
	*****/
	app.post('/folders/:id/files', upload.single('file'),  async(req, res) => {

		try{

			//check if valid mongo id
			if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
				return res.send( { status: false, message: "invalid Id" } );
			}

			let folder = await Folder.findOne( {_id: req.params.id } );

			//check if folder exists in db
			if( folder === null ){
				return res.send( { status: false, message: "Folder not Found" } );
			}

			//check if owner
			if( req.current_user._id.toString() !== folder.user.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			if( req.file !== undefined ){

				let file = new File();
				file.folder = req.params.id;
				file.name = req.file.originalname;
				file.type = req.file.mimetype;
				file.path = req.file.path;
				file.user = req.current_user._id;

				await file.save();

				return res.send( { status: true, message: "File successfully uploaded", data: file } );

			}
			else{
				return res.send( { status: false, message: "No file was uploaded" } );
			}

			

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

	/***
	* List all files in folder
	**/
	app.get('/folders/:slug/files', async(req, res) => {

		try{
			//check if valid mongo id
			// if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
			// 	return res.send( { status: false, message: "invalid Id" } );
			// }

			let folder = await Folder.findOne( {slug: req.params.slug } );

			//check if folder exists in db
			if( folder === null ){
				return res.send( { status: false, message: "Folder not Found" } );
			}

			//check if owner
			if( req.current_user._id.toString() !== folder.user.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			let files = await File.find( {folder: folder.id} );

			return res.send( { status: true, data: { files: files, folder: folder } } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}

	});

}