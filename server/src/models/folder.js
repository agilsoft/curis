const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const slugify = require('slugify');
 
let schema = new Schema({
	name: String,
	slug: String,
	user: ObjectId
},
{
  	timestamps: true
});


schema.pre('save', function(next) {
  this.increment();
  this.slug = slugify(this.name, {lower: true});
  return next();
});


module.exports = schema;