import React, { Component } from 'react';
import _ from 'lodash';

import { connect } from 'react-redux';

import { createNewFolder } from '../../actions/folders';

import {
    FormBuilder,
    FieldGroup,
    FieldControl,
    Validators,
 } from "react-reactive-form";

import { TextInput } from '../../utils/inputs';

class New extends Component {

	constructor(props){
		super(props);

		this.folderForm = FormBuilder.group({
	        name: ["", Validators.required]
	    });
	}

	componentWillReceiveProps(){

	}

	handleSubmit = (e) => {
		e.preventDefault();

		if(this.folderForm.status == "VALID"){

			let { name } = this.folderForm.value;
			this.props.createNewFolder( name );

			this.folderForm.reset();
		}

		

	}

	render(){
		return(

			<div className="modal fade" id="newFolderModal" tabIndex="-1" role="dialog" aria-hidden="true">
	            <div className="modal-dialog" role="document">
	                <div className="modal-content">
	                    <div className="modal-header">
	                        <h5 className="modal-title" id="exampleModalLabel">Add New Folder</h5>
	                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div className="modal-body">

	                    	<FieldGroup
			                control={this.folderForm}
			                render={({ get, invalid }) => (
			                  	<form id="newFolder" onSubmit={this.handleSubmit}>

	                     			<FieldControl
				                      name="name"
				                      render={ TextInput }
				                      meta={{ label: "Folder Name" }}
				                    />
	                        	</form>
                        	) } />

	                    </div>
	                    <div className="modal-footer">
	                        
	                        <button type="submit" form="newFolder" className="btn btn-primary">Create Folder</button>
	                    </div>
	                </div>
	            </div>
	        </div>
		)
	}

}

// const mapStateToProps = (store) => {
//     return {
//         data: store.folders.response
//     }
// }

const mapDispatchToProps = (dispatch) => {
    return {
        createNewFolder: (name) => dispatch( createNewFolder(name) )
    }
}

export default connect(
  null,
  mapDispatchToProps
)(New);