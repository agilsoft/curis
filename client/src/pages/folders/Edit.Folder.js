import React, { Component } from 'react';

import { connect } from 'react-redux';

import {
    FormBuilder,
    FieldGroup,
    FieldControl,
    Validators,
 } from "react-reactive-form";

 import { TextInput } from '../../utils/inputs';

 import { renameFolder } from '../../actions/folders';

class Edit extends Component {

	constructor(props){
		super(props);
		this.folderForm = FormBuilder.group({
	        name: ["", Validators.required]
	    });
	}

	handleSubmit = (e) => {
		e.preventDefault();

		if(this.folderForm.status == "VALID"){

			let { name } = this.folderForm.value;
			this.props.renameFolder( this.props.folder._id, name );
		}

	}

	componentWillReceiveProps(props){

		if( props.folder !== null ){
			this.folderForm.setValue( { name: props.folder.name } )
		}
		
	}

	render(){
		return(
			<div className="modal fade" id="editFolderModal" tabIndex="-1" role="dialog" aria-hidden="true">
	            <div className="modal-dialog" role="document">
	                <div className="modal-content">
	                    <div className="modal-header">
	                        <h5 className="modal-title" id="exampleModalLabel">Rename Folder</h5>
	                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div className="modal-body">

	                    	<FieldGroup
			                control={this.folderForm}
			                render={({ get, invalid }) => (
			                  	<form id="renameFolder" onSubmit={this.handleSubmit}>

	                     			<FieldControl
				                      name="name"
				                      render={ TextInput }
				                      meta={{ label: "Folder Name" }}
				                    />
	                        	</form>
                        	) } />

	                    </div>
	                    <div className="modal-footer">
	                        <button type="submit" form="renameFolder" className="btn btn-primary">Rename</button>
	                    </div>
	                </div>
	            </div>
	        </div>
		)
	}


}


const mapDispatchToProps = (dispatch) => {
    return {
        renameFolder: (id, name) => dispatch( renameFolder(id, name) )
    }
}

export default connect(
  null,
  mapDispatchToProps
)(Edit);