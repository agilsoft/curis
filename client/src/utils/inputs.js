import React from 'react';

export const TextInput = ({ handler, touched, hasError, meta }) => (
  <div>

  	<label className="form-group has-top-label">
        <input type="text" className="form-control" {...handler()} />
        <span>{ meta.label }</span>
    </label>
    <div className="feedback-custom invalid-feedback">
        {touched
        && hasError("required")
        && `${meta.label} is required`}
    </div>
    
  </div>  
)

export const TextInputPlain = ({ handler, touched, hasError, meta }) => (
  <div>
  	<label htmlFor={meta.id}>{ meta.label }</label>
    <input type="text" 
            className="form-control" 
            id={meta.id} 
            placeholder={meta.label} 
            {...handler()} />
    <div className="feedback-custom invalid-feedback mt-0">
        {touched
        && hasError("required")
        && `${meta.label} is required`}
    </div>
  </div>  
)

export const PasswordInput = ({ handler, touched, hasError, meta }) => (
  <div>
    <label htmlFor={meta.id}>{ meta.label }</label>
    <input type="password" 
            className="form-control" 
            id={meta.id} 
            placeholder={meta.label} 
            {...handler()} />
    <div className="feedback-custom invalid-feedback mt-0">
        {touched
        && hasError("required")
        && `${meta.label} is required`}
    </div>
  </div>  
)

export const SelectInput = ({ handler, touched, hasError, meta }) => (

	<div>
		<label htmlFor={meta.id}>{ meta.label }</label>
        <select id="inputState" className="form-control">
            <option>Choose...</option>
            <option>...</option>
        </select>
	</div>

)