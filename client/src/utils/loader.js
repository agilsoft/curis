import emitter from './emitter';

export function startLoader(){
 	emitter.emit( 'loading:start' );
}

export function stopLoader(){
 	emitter.emit( 'loading:stop' );
}