import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    FormBuilder,
    FieldGroup,
    FieldControl,
    Validators,
 } from "react-reactive-form";

 import { PasswordInput } from '../../utils/inputs';
 import { resetPassword, getMyProfile } from '../../actions/auth';
 import notify from '../../utils/notify';

class Password extends Component {


	constructor(props){
		super(props);


		this.passwordForm = FormBuilder.group({
	        new: ["", Validators.required],
	        confirm: ["", Validators.required]
		})

	}

	componentWillReceiveProps(props){
	
		if( props.error && props.error !== null){
			notify.error("Error", this.props.error)
		}

		//check for response in store
		if( props.response !== null  ){
			let { status, message } = props.response;

			if( !status ){
				notify.error( "Error", message );
			}

			if( status ){
				notify.success( "Success", message );
				props.getMyProfile();
			}
		}

	}

	strongPassword(str)
	{
	    // at least one number, one lowercase and one uppercase letter
	    // at least six characters
	    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
	    return re.test(str);
	}

	handleSubmit = (e) => {
		e.preventDefault();
		if(this.passwordForm.status == "VALID"){

			if( this.passwordForm.value.new !== this.passwordForm.value.confirm ){
				notify.error( "Error", "Confirm password should match new password" );
				return false;
			}

			if( !this.strongPassword( this.passwordForm.value.new ) ){
				notify.error( "Error", "Enter a strong password." );
				return false;
			}

			this.props.resetPassword( this.passwordForm.value );

			this.passwordForm.reset();
		}
	}

	render() {

		let { user } = this.props;

	    return (
	    	<div>
				<h1>Password</h1>
				<div className="separator mb-5"></div>

				<div className="card mb-4">
			        <div className="card-body">

			        	<FieldGroup
				                control={this.passwordForm}
				                render={({ get, invalid }) => (

				                	<form id="chnagePassword" onSubmit={this.handleSubmit} >
				                		
				                		{ user && !user.is_google && <div className="form-row">
							          		<div className="form-group col-md-12">
							         			<FieldControl
							                      name="current"
							                      options={ { validators: Validators.required } }
							                      render={ PasswordInput }
							                      meta={{ label: "Current Password", id: "current" }}
							                    />
							                </div>
							            </div> }

							            <div className="form-row">
							          		<div className="form-group col-md-12">
							         			<FieldControl
							                      name="new"
							                      render={ PasswordInput }
							                      meta={{ label: "New Password", id: "new" }}
							                    />

							                    <small id="passwordHelp" className="form-text text-muted">
							                    	Minimum 6 characters containing at least one number, one lowercase and one uppercase letter
							                    </small>
							                </div>
							            </div>

							            <div className="form-row">
							          		<div className="form-group col-md-12">
							         			<FieldControl
							                      name="confirm"
							                      render={ PasswordInput }
							                      meta={{ label: "Confirm Password", id: "confirm" }}
							                    />
							                </div>
							            </div>

							            <button disabled={invalid} type="submit" className="update btn btn-primary d-block mt-3">Update</button>

				                	</form>

				            ) } />

			            
			        </div>
			    </div>
			</div>
	    );
	}
}


const mapStateToProps = (store) => {
    return {
        user: store.auth.data,
        error: store.auth.error,
        response: store.auth.response
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetPassword: (params) => dispatch( resetPassword(params) ),
        getMyProfile: () => dispatch( getMyProfile() )
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Password);
