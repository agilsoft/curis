import React from 'react';

import _ from 'lodash';

const Actions = (props) => {

	let actions = null;

	if(props.actions && props.actions.length > 0){
		actions = _.map( props.actions, (action, k) => {
			return <button key={k} type="button" className="btn btn-primary btn-lg top-right-button mr-1 btn-action" onClick={action.callback}>{action.title}</button>
		})
	}

	return actions;
}

const BulkActions = (props) => {

	if( props.actions && props.actions.length > 0){
		return(

			<div className="btn-group ">
	            <div className="btn btn-primary btn-lg pl-4 pr-0 check-button">
	                <label className="custom-control custom-checkbox mb-0 d-inline-block">
	                    <input type="checkbox" className="custom-control-input" id="checkAll" />
	                    <span className="custom-control-label"></span>
	                </label>
	            </div>
	            <button type="button" className="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split pl-2 pr-2"
	                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                <span className="sr-only">Toggle Dropdown</span>
	            </button>
	            <div className="dropdown-menu dropdown-menu-right">
	            { _.map(props.actions, (action, k) => {
	            	return <a key={k} className="dropdown-item" href="javascript:;" onClick={ action.callback } >{action.title}</a>
	            } ) }
	            </div>
	            
	        </div>
		)
	}
	else{
		return null;
	}

	
}

const DisplayOptions = (props) => {

	return(
		<div className="collapse d-md-block" id="displayOptions">
			<a className="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
	            role="button" aria-expanded="true" aria-controls="displayOptions">
	            Display Options
	            <i className="simple-icon-arrow-down align-middle"></i>
	        </a>


	        { props.actions && props.actions.length > 0 && <div className="d-block d-md-inline-block">
	            <div className="btn-group float-md-left mr-1 mb-1">
	                <button className="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
	                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                    Sort By
	                </button>
	                <div className="dropdown-menu">
	                { _.map( props.actions, (action, k) => {
	                	return <a key={k} className="dropdown-item" href="javascript:;" onClick={ action.callback }>{ action.title }</a>
	                } ) }
	                </div>
	            </div>
	            
	        </div> }

	        { props.pagination && 
		        <div className="float-md-right">
		            <span className="text-muted text-small">Displaying 1-10 of 210 items </span>
		            <button className="btn btn-outline-dark btn-xs dropdown-toggle" type="button" data-toggle="dropdown"
		                aria-haspopup="true" aria-expanded="false">
		                20
		            </button>
		            <div className="dropdown-menu dropdown-menu-right">
		                <a className="dropdown-item" href="javascript:;" onClick={ () => { props.onChangePerPage(10) } }>10</a>
		                <a className="dropdown-item" href="javascript:;" onClick={ () => { props.onChangePerPage(20) } }>20</a>
		                <a className="dropdown-item" href="javascript:;" onClick={ () => { props.onChangePerPage(30) } }>30</a>
		                <a className="dropdown-item" href="javascript:;" onClick={ () => { props.onChangePerPage(50) } }>50</a>
		                <a className="dropdown-item" href="javascript:;" onClick={ () => { props.onChangePerPage(100) } }>100</a>
		            </div>
		        </div>
		    }
        </div>
	)

} 

const PageHeader = (props) => {

	return(
		<div className="row">
            <div className="col-12">
                <div className="mb-2">
                    <h1>{ props.title }</h1>
                    <div className="float-sm-right text-zero">

                    	<Actions actions={ props.actions } />
                    	<BulkActions actions={ props.bulkActions } />

                    </div>
                    
                </div>

                <div className="mb-2">
                	<DisplayOptions actions={ props.sortActions } onChangePerPage={ props.onChangePerPage } />
                </div>
                <div className="separator mb-5"></div>
            </div>
        </div>
	);

}

export default PageHeader;