# Curis File Manager

A simple file manager app built using **NodeJS**, **ReactJs** and **MongoDB**

**NodeJS** source is placed under ```/server``` directory
**ReactJS** source is placed under ```/client``` directory

To Start the App:

NodeJS app can be started in development using following commands. You can use npm as well.
```
cd server
yarn install
yarn dev
```

To Start ReactJS App enter the following commands:
```
cd client
yarn install
yarn start
```

