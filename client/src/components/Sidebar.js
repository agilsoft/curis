import React, { Component } from 'react';

import { Link } from "react-router-dom";

class Sidebar extends Component {

	signOut = () => {
		window.localStorage.removeItem('jwt');
		window.location.href="/login";
	}

	render() {
	    return (
	    	<div className="sidebar">
		        <div className="main-menu">
		            <div className="scroll">
		                <ul className="list-unstyled">
		                    <li className="active">

		                    	<Link to="/files">
		                    		<i className="simple-icon-folder-alt"></i>
		                            <span>Data</span>
		                    	</Link>

		                        {/*<a href="#files">
		                            <i className="simple-icon-folder-alt"></i>
		                            <span>Files</span>
		                        </a> */ }
		                    </li>
		                    <li>
		                        {/*<a className="has-submenu" href="#settings">*/}
		                        <Link to="/settings/account">
		                            <i className="simple-icon-settings"></i> Profile
		                        </Link>
		                    </li>
		                    <li>
		                    	<Link to="/settings/password">
		                    		<i className="simple-icon-lock"></i> Settings
		                    	</Link>
		                    </li>
		                    <li>
		                        <a href="javascript:;" onClick={ this.signOut }>
		                            <i className="simple-icon-logout"></i> Sign Out
		                        </a>
		                    </li>
		                    
		                </ul>
		            </div>
		        </div>

		        <div className="sub-menu">
		            <div className="scroll">
		                <ul className="list-unstyled" data-link="files">
		                    <li className="">

		                    	<Link to="/files">
		                    		<i className="simple-icon-docs"></i> My Files
		                    	</Link>

		                    </li>
		                    <li>
		                        <a href="New.File.html">
		                            <i className="simple-icon-doc"></i> New File
		                        </a>
		                    </li>
		                    <li>
		                        <a href="New.Folder.html">
		                            <i className="simple-icon-folder"></i> New Folder
		                        </a>
		                    </li>
		                </ul>

		                <ul className="list-unstyled" data-link="settings">
		                    <li>
		                    	<Link to="/settings/account">
		                    		<i className="simple-icon-user"></i> Profile
		                    	</Link>
		                    </li>
		                    <li>
		                    	<Link to="/settings/password">
		                    		<i className="simple-icon-lock"></i> Password
		                    	</Link>
		                    </li>
		                </ul>
		                
		            </div>
		        </div>
		    </div>
	    );
	}
}

export default Sidebar;