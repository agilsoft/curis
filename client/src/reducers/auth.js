import { AUTH as TYPE } from '../constants';


export default function reducer( state  = {
	error: null,
	data: null,
	response: null
}, action){

	switch(action.type){

		case `${TYPE}_PROFILE_PENDING`:
			return {...state, error: null, response: null}
		case `${TYPE}_PROFILE_FULFILLED`:
			return {...state, data: action.payload.data.data, error: null }
		case `${TYPE}_PROFILE_REJECTED`:
			return {...state, error: action.payload }


		case `${TYPE}_PENDING`:
		case `${TYPE}_PROFILE_UPDATE_PENDING`:
		case `${TYPE}_PASSWORD_PENDING`:
			return {...state, error: null, response: null}
		case `${TYPE}_FULFILLED`:
		case `${TYPE}_PROFILE_UPDATE_FULFILLED`:
		case `${TYPE}_PASSWORD_FULFILLED`:
			return {...state, response: action.payload.data, error: null, }
		case `${TYPE}_REJECTED`:
		case `${TYPE}_PROFILE_UPDATE_REJECTED`:
		case `${TYPE}_PASSWORD_REJECTED`:
			return {...state, error: action.payload }

	}

	return state;

}