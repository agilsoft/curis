import { API_URL } from '../config';
import axios from 'axios';

import { AUTH as TYPE } from '../constants';

/*** Authorization Header */
let axiosConfig = {
  	headers: {
      'Authorization': 'Bearer ' + window.localStorage.getItem('jwt')
    }
};

export function googleLogin(code){
	return {
		type: `${TYPE}`,
		payload: axios.get( API_URL + '/google/auth?code=' + code )
	}
}

export function getMyProfile(){
	return {
		type: `${TYPE}_PROFILE`,
		payload: axios.get( API_URL + '/users/profile', axiosConfig )
	}
}

export function updateProfile(params){
	return {
		type: `${TYPE}_PROFILE_UPDATE`,
		payload: axios.put( API_URL + '/users/profile', params,  axiosConfig )
	}
}

export function resetPassword(params){
	return {
		type: `${TYPE}_PASSWORD`,
		payload: axios.put( API_URL + '/users/password', params,  axiosConfig )
	}
}