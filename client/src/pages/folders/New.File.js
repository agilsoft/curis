import React, { Component } from 'react';
import { connect } from 'react-redux';

import { API_URL } from '../../config';

class New extends Component {

	constructor(props){
		super(props);

	}

	componentWillMount(){

		setTimeout( () => {

			if( !window.mydropzone ){

				window.mydropzone = new window.Dropzone(".dropzone", { 
					url: `${API_URL}/folders/${this.props.folder._id}/files`,
					headers: {
			      		'Authorization': 'Bearer ' + window.localStorage.getItem('jwt')
				    },
				    success: ( file, response ) => {
				    	this.props.onFIleUpload(response);
				    }
				 })

			}

			 
		})

	
	}

	render(){
		return(

			<div className="modal fade" id="newFileModal" tabIndex="-1" role="dialog" aria-hidden="true">
	            <div className="modal-dialog" role="document">
	                <div className="modal-content">
	                    <div className="modal-header">
	                        <h5 className="modal-title" id="exampleModalLabel">Upload File</h5>
	                        
	                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div className="modal-body">
	                    	<div className="mb-2">
	                        	<small className="text-muted">Uploading to { this.props.folder.name }</small>
	                        </div>
	                    	<div className="card drop-area">
		                        <div className="card-body">
		                            <form action="/file-upload">
		                                <div className="dropzone ">
		                                </div>
		                            </form>
		                        </div>
		                    </div>

	                    </div>
	                    <div className="modal-footer">
	                        
	                        <button type="submit" form="uploadFile" className="btn btn-primary">Upload</button>
	                    </div>
	                </div>
	            </div>
	        </div>
		)
	}

}

export default New;