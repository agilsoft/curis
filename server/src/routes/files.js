var mongoose = require('mongoose');

const db = require('../db');
const File = db.model('File');

var fs = require('fs');
const { promisify } = require('util')
const unlinkAsync = promisify(fs.unlink)

module.exports = function(app){

	/***
	* View File
	**/
	app.get('/files/:id', async(req, res) => {
		
		try{
			//check if valid mongo id
			if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
				return res.send( { status: false, message: "invalid Id" } );
			}

			let file = await File.findOne( {_id: req.params.id } );

			//check if file exists in db
			if( file === null ){
				return res.send( { status: false, message: "File not Found" } );
			}

			//check if owner
			if( req.current_user._id.toString() !== file.user.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			res.send( { status: true, data: file } );

		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}



	});

	/**
	* Download File
	***/
	app.get('/files/:id/download', async(req, res) => {

		//check if valid mongo id
		if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
			return res.send( { status: false, message: "invalid Id" } );
		}

		let file = await File.findOne( {_id: req.params.id } );

		//check if file exists in db
		if( file === null ){
			return res.send( { status: false, message: "File not Found" } );
		}

		//check if owner
		if( req.current_user._id.toString() !== file.user.toString() ){
			return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
		}
		

		let contents = fs.readFileSync( file.path, 'base64');
		//let buffer = new Buffer(contents, 'base64');

		res.setHeader("Content-Type", file.type );
		//res.setHeader("Content-Length", buffer.length );
		res.send(contents);

	})

	/***
	*	Rename the file
	*   params: name
	**/
	app.put('/files/:id', async(req, res) => {

		//check if valid mongo id
		if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
			return res.send( { status: false, message: "invalid Id" } );
		}
		
		let file = await File.findOne( {_id: req.params.id } );

		//check if owner
		if( req.current_user._id.toString() !== file.user.toString() ){
			return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
		}


		if( req.body.name ){
			file.name = req.body.name;
			await file.save();
		}
		

		res.send( { status: true, message: "File successfully updated", data: file } );

		//check if file exists in db
		if( file === null ){
			return res.send( { status: false, message: "File not Found" } );
		}

	});

	/***
	* Delete the file
	**/
	app.delete('/files/:id', async(req, res) => {
		try{
			//check if valid mongo id
			if( !mongoose.Types.ObjectId.isValid(req.params.id) ){
				return res.send( { status: false, message: "invalid Id" } );
			}

			let file = await File.findOne( {_id: req.params.id } );

			//check if file exists in db
			if( file === null ){
				return res.send( { status: false, message: "File not Found" } );
			}

			//check if owner
			if( req.current_user._id.toString() !== file.user.toString() ){
				return res.status(401).send( { status: false, message: "You are not allowed to perform this action" } );
			}

			// Delete the file like normal
    		await unlinkAsync(file.path)
    		
			await file.remove();

			res.send( { status: true, message: "File successfully deleted" } );
		}
		catch(e){
			res.send( { status: false, message: e.message } );
		}
	})


}