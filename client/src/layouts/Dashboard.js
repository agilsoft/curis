import React, { Component } from 'react';
import { connect } from 'react-redux';

import Nav from '../components/Nav';
import Sidebar from '../components/Sidebar';

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import Folders from '../pages/Folders';
import Files from '../pages/folders/Files';
import AccountSettings from '../pages/settings/Account';
import PasswordSettings from '../pages/settings/Password';

import { getMyProfile } from '../actions/auth';

class Dashboard extends Component {

	componentWillMount(){
		this.props.getMyProfile();
	}

	render() {
	    return (
	    	<Router>
		    	<div className="wrapper">
	    			<Nav />
	    			<Sidebar />
		    		<main>
		    			<div className="container-fluid disable-text-selection">
		    			
		    				<Route exact path="/files" component={ Folders } />
		    				<Route exact path="/files/:folder" component={ Files } />
		    				<Route exact path="/settings/account" component={ AccountSettings } />
		    				<Route exact path="/settings/password" component={ PasswordSettings } />

		    				
		    			</div>
		    		</main>
		    		
		    	</div>
	    	</Router>
	 
	    );
	}
}


/* Redux */
const mapStateToProps = (store) => {
    return {
        
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMyProfile: () => dispatch( getMyProfile() )
    }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)